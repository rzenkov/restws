package main

import (
	"encoding/json"
	"github.com/gorilla/websocket"
	"io/ioutil"
	"log"
	"math/rand"
	"net/http"
)

type Client struct {
	Conn *websocket.Conn
	Msgs chan string
}

type Msg struct {
	Text    string
	Addr    string
	Session string
}

var (
	regChan   = make(chan *Client)
	unregChan = make(chan *websocket.Conn)
	messages  = make(chan string)
	clients   = make(map[*websocket.Conn]*Client)
	upgrader  = &websocket.Upgrader{
		ReadBufferSize:  1024,
		WriteBufferSize: 1024,
		CheckOrigin:     func(r *http.Request) bool { return true },
		Subprotocols:    []string{"soap", "wamp"},
	}
	sessNames = []string{"pink", "red", "blue", "green", "black", "white"}
)

func WsHandler(w http.ResponseWriter, r *http.Request) {
	log.Println("Start connection")
	ws, err := upgrader.Upgrade(w, r, nil)
	//protos := websocket.Subprotocols(r)
	//proto := ws.Subprotocol()
	//log.Println(protos, proto)
	log.Println("Upgrade connection")
	if err != nil {
		log.Println(err)
		return
	}
	msgs := make(chan string)
	regChan <- &Client{ws, msgs}
	defer func() {
		log.Println("Close connection")
		unregChan <- ws
	}()
	sessName := sessNames[rand.Int31n(5)]
	go func() {
		for msg := range msgs {
			log.Println("Send message")
			ws.WriteMessage(websocket.TextMessage, []byte(msg))
		}
	}()
	for {
		if t, msg, err := ws.ReadMessage(); err == nil {
			// send message from ws
			if ws.Subprotocol() == "wamp" {
				addr := ws.RemoteAddr().String()
				if text, err := json.Marshal(Msg{Text: string(msg), Addr: addr, Session: sessName}); err == nil {
					log.Println("Recieve message")
					messages <- string(text)
				}
			} else {
				messages <- string(msg)
			}
		} else {
			log.Println("Error when recieve message", err, t)
			break
		}
	}
}

func RestHandler(w http.ResponseWriter, r *http.Request) {
	msg, err := ioutil.ReadAll(r.Body)
	log.Println(r.Header.Get("Content-Type"))
	defer r.Body.Close()
	if err != nil {
		// need to response with error?
		log.Println("Error when reading request body", err)
		http.Error(w, "Request parse error", http.StatusInternalServerError)
		return
	}
	addr := r.RemoteAddr
	if text, err := json.Marshal(Msg{Text: string(msg), Addr: addr}); err == nil {
		messages <- string(text)
	}
	w.Header().Add("Content-Type", "application/json")
	w.Write([]byte("Ok"))
}

func dispatch() {
	for {
		select {
		case client := <-regChan:
			clients[client.Conn] = client
		case conn := <-unregChan:
			if client, ok := clients[conn]; ok {
				close(client.Msgs)
			}
			delete(clients, conn)
		case msg := <-messages:
			for _, client := range clients {
				client.Msgs <- msg
			}
			log.Println("message send for ", len(clients), " clients")
		}
	}
}

func main() {
	go dispatch()
	http.HandleFunc("/ws", WsHandler)
	http.HandleFunc("/rest", RestHandler)
	log.Fatal(http.ListenAndServe(":9090", nil))
}
